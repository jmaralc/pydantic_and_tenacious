import abc
import datetime
import random
from typing import Literal, List
from pydantic import BaseModel


class Order(BaseModel):
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[str]


    @abc.abstractmethod
    def allocate_elements(self):
        raise NotImplementedError


class HardwareOrder(Order):
    allocation_states: List[Literal["PROCESSING", "FINISHED", "FAILED"]] = []

    def allocate_elements(self):
        for element in self.elements:
            self.allocation_states.append(random.choice(["PROCESSING", "FINISHED", "FAILED"]))


if __name__ == "__main__":
    new_order = HardwareOrder(
        order_id=1,
        country="spain",
        order_at=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=["10 nails", "1 red paint bottle"]
    )

    new_order.allocate_elements()

    for element, state in zip(new_order.elements, new_order.allocation_states):
        print(f"\tThe element {element} is in state {state}")
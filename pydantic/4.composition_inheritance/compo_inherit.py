import datetime
from typing import Literal, List, Union
from pydantic import BaseModel, ValidationError


class Element(BaseModel):
    name: str
    amount: int

class ExplosiveElement(Element):
    explosivity: Literal["HIGH", "MEDIUM", "LOW"]


class Order(BaseModel):
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[Union[Element, ExplosiveElement]]

if __name__ == "__main__":
    new_order = Order(
        order_id=1,
        country="spain",
        order_at=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=[
            Element(name="nail", amount=10),
            Element(name="red paint", amount=1),
            ExplosiveElement(name="TNT", amount=5, explosivity="HIGH")
        ]
    )

    print("**This is a VALID order:")
    print(f"\t{new_order}")

    print(f"**This is a INVALID order:")
    try:
        new_order.elements.append(
            ExplosiveElement(name="nitroglycerin", amount=1, explosivity="UNKNOWN")
        )
    except ValidationError as error:
        print(error)
        print("The same error but with json format ready for your API:")
        print(error.json())

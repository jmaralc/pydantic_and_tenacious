import datetime
from typing import Literal, List, Union

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Element(BaseModel):
    name: str
    amount: int

class ExplosiveElement(Element):
    explosivity: Literal["HIGH", "MEDIUM", "LOW"]


class Order(BaseModel):
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[Union[Element, ExplosiveElement]]



@app.post("/order")
async def process_order(order: Order):
    print(order)
    return {"message": "your order has been processed"}


if __name__ == "__main__":
    uvicorn.run(app)
import datetime
from typing import List
from pydantic import BaseModel


class Order(BaseModel):
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[str]


    # This will work similar to the frozen parameter of dataclasses
    class Config:
        allow_mutation = False


if __name__ == "__main__":
    new_order = Order(
        order_id=1,
        country="spain",
        order_at=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=["10 nails", "1 red paint bottle"]
    )

    try:
        new_order.country = "colombia"
    except TypeError as error:
        print(error)
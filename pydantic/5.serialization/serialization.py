import datetime
import humps
from typing import List
from pydantic import BaseModel


class Order(BaseModel):
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[str]

    class Config:
         # This defines the callback function for generating the aliases
        alias_generator = humps.camelize
        allow_population_by_field_name = True

if __name__ == "__main__":

    # Properties in snake case
    new_order = Order(
        order_id=1,
        country="spain",
        order_at=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=["10 nails", "1 red paint bottle"]
    )

    print("**This is a VALID order:")
    print(f"\t{new_order}")

    print("**This is a VALID order already parsed and presented as json:")
    print(f"\t{new_order.json(by_alias=True)}")

    print("=====================================")
    new_order = Order(
        orderId=1,
        country="spain",
        orderAt=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=["10 nails", "1 red paint bottle"]
    )

    print("**This is a VALID order:")
    print(f"\t{new_order}")

    # Imagine that we received the data as a json from a external service
    # and the keys are in camelCase...
    external_body = {
        "orderId":2,
        "country": "spain",
        "orderAt": "2020-10-20T01:58:18.396931",
        "description": "First sale",
        "elements": ["10 arduino"]
    }
    
    external_order = Order.parse_obj(external_body)
    print("**The external order is a VALID order:")
    print(f"\t{external_order}")

    external_order = Order(**external_body)
    print("**The same order can be modeled in our domain with different methods, but same result:")
    print(f"\t{external_order}")

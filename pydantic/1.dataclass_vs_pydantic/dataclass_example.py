from typing import List
import datetime
from dataclasses import dataclass


@dataclass
class Order:
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[str]



if __name__ == "__main__":
    # mypy will ensure the object below is correct
    new_order = Order(
        1,
        "spain",
        datetime.datetime.now(),
        "Rodriguez workshop",
        ["10 nails", "1 red paint bottle"]
    )
    print(f"**This is a VALID order:\n\t{new_order}")


    # mypy will detect that there is an error with the object below...
    # but there will be no error at runtime
    new_order = Order(
        1.3333,  # This parameter should not get a float
        "spain",
        datetime.datetime.now(),
        "Rodriguez workshop",
        ["10 nails", "1 red paint bottle"]
    )
    print(f"**This is an INVALID order:\n\t{new_order}")

    # This is a more extreme case...that will not fail in runtime
    new_order = Order(
        [1],  # This parameter should not get a list
        "spain",
        datetime.datetime.now(),
        "Rodriguez workshop",
        ["10 nails", "1 red paint bottle"]
    )
    print(f"**This is an INVALID order:\n\t{new_order}")
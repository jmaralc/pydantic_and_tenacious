from typing import List
import datetime
from pydantic import BaseModel, ValidationError


class Order(BaseModel):
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[str]



if __name__ == "__main__":
    # mypy will ensure the object below is correct
    new_order = Order(
        order_id=1,
        country="spain",
        order_at=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=["10 nails", "1 red paint bottle"]
    )
    print(f"**This is a VALID order:\n\t{new_order}")


    # mypy will detect that there is an error with the object below...
    # but there will be no error at runtime
    new_order = Order(
        order_id=1.3333,    # This parameter should not get a float
        country="spain",
        order_at=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=["10 nails", "1 red paint bottle"]
    )
    print("**This is a VALID order BUT if we wanted to keep the order as float we lost that information:")
    print(f"\t{new_order}")

    # mypy will detect that there is an error with the object below...
    # but there will be no error at runtime
    try:
        new_order = Order(
            order_id=[1],  # This parameter should not get a list
            country="spain",
            order_at=datetime.datetime.now(),
            description="Rodriguez workshop",
            elements=["10 nails", "1 red paint bottle"]
        )
    except ValidationError as error:
        print(f"**This is a INVALID order that can not be parsed. The reason for that is:")
        print(error)
        print("The same error but with json format ready for your API:")
        print(error.json())

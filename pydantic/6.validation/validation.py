import datetime
from typing import List
from pydantic import BaseModel, ValidationError, validator


class Order(BaseModel):
    order_id: int
    country: str
    order_at: datetime.datetime
    description: str
    elements: List[str]

    @validator('elements')
    def peacekeeper(cls, v, values, **kwargs):
        if  any([el for el in v if "gun"in el]):

            raise Exception(
                f"The customer {values.get('description')} is trying to buy a GUN!!!"
            )
        return v

if __name__ == "__main__":
    new_order = Order(
        order_id=1,
        country="spain",
        order_at=datetime.datetime.now(),
        description="Rodriguez workshop",
        elements=["10 nails", "1 red paint bottle"]
    )

    print("**This is a VALID order:")
    print(f"\t{new_order}")

    try:
        new_order = Order(
            order_id=1,
            country="spain",
            order_at=datetime.datetime.now(),
            description="Kingpin",
            elements=["10 shotguns"]
        )
    except Exception as error:
        print(f"**This is a INVALID order that can not be parsed. The reason for that is:")
        print(error)

def hello_with_hints(name: str) -> str:
    return "\tHello " + name


def hello_without_hints(name):
    return "\tHello " + name


if __name__ == "__main__":
    
    print("The next execution will not fail")
    print(f"{hello_with_hints('TypedJavier')}")

    print("The next execution will  not fail")
    print(f"{hello_without_hints('UntypedJavier')}")

    ## You will be warned by mypy about the next line
    print(f"{hello_with_hints(3)}")

    ##You will NOT be warned by mypy about the next line
    ## But it will crash in runtime
    print(f"{hello_without_hints(3)}")

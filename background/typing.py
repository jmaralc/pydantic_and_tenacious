import json
import random


list_possible_values = ["e", 5, 5.6, {"a": 5}, None]

def dynamic_typing_example():
    result = random.choice(list_possible_values)
    result_type = str(type(result)).upper()
    print(f"\tThe result is {result} of type {result_type}")


def static_typing_example():
    result = random.choice(list_possible_values)
    result_type = str(type(result)).upper()

    if not isinstance(result, int):
        print(f"Error! Not valid type for {result}. Expecting int but got {result_type}")
    else:
        print(f"\tThe result is {result} of type {result_type}")



class myInt(int):
    n = 0

    def __iter__(self,):
        self.n = 0
        return self

    def __next__(self):
        if self.n < len(self.__str__()):
            result = self.__str__()[self.n]
            self.n += 1
            return result
        else:
            raise StopIteration

def duck_typing_example():
    result = random.choice(list_possible_values)
    result_type = str(type(result)).upper()

    if not isinstance(result, int):
        print(f"\tError! Not valid type for {result}. Expecting int but got {result_type}")
    else:
        print(f"\tThe result is {result} of type {result_type}")


if __name__ == "__main__":
    
    print("**Running dynamic typing example")
    for exacutions in range(5):
        dynamic_typing_example()
    print("========================================")
    print("**Running 'static' typing example")
    for exacutions in range(5):
        static_typing_example()
    print("========================================")
    print("**Runing duck typing example")
    my_number1 = myInt(353)
    my_number2 = myInt(7)

    print(f"\tWe can add two of my custom integers {my_number1} and {my_number2}")
    print(f"\tThe result is {my_number1+my_number2}")
    print(f"\tBut they are not type int, they are {type(my_number1)}")
    print("""
        They are not type iterators, in fact an integer int can not act as an iterator
        The definition of an iterator is to have an __iter__ and a __next__ method.
    """)
    for digit  in my_number1:
        print(f"\tDigit:{digit}")

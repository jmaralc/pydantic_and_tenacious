import random
from tenacity import retry

class NoisyChannel:

    @retry
    def send(self, message: str):
        print("Sending the message...")
        error = random.choices([True, False], weights = [10, 1], k = 1)

        if error[0]:
            raise IOError("Connection is broken!")


def answer_with_message(message: str):
    comm_interface = NoisyChannel()
    comm_interface.send(message)
    return "Message sent"

if __name__ == "__main__":

    message = input("Leave a message after the signal. BEEEEP!")
    response = answer_with_message(message)
    print(response)
import random
from tenacity import retry, stop_after_attempt

class NoisyChannel:

    @retry(stop=stop_after_attempt(4))
    # @retry(stop=(stop_after_delay(10) | stop_after_attempt(5)))
    # @retry(wait=wait_exponential(multiplier=1, min=4, max=10))
    # @retry(retry=retry_if_exception_type(IOError))
    # @retry(retry=(retry_if_result(None) | retry_if_exception_type(IOError)))
    def send(self, message: str):
        print("Sending the message...")
        error = random.choices([True, False], weights = [10, 1], k = 1)

        if error[0]:
            raise IOError("Connection is broken!")


def answer_with_message(message: str):
    comm_interface = NoisyChannel()
    try:
        comm_interface.send(message)
    except Exception:
        pass
    import pdb; pdb.set_trace()
    print()
    return "Message sent"

if __name__ == "__main__":

    message = input("Leave a message after the signal. BEEEEP!")
    response = answer_with_message(message)
    print(response)
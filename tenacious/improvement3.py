import asyncio
import random
from tenacity import AsyncRetrying, RetryError, stop_after_attempt

class NoisyChannel:

    async def send(self, message: str):
        try:
            async for attempt in AsyncRetrying(stop=stop_after_attempt(2)):
                with attempt:
                    print("Sending the message...")
                    error = random.choices([True, False], weights = [50, 1], k = 1)

                    if error[0]:
                        raise IOError("Connection is broken!")
        except RetryError:
            pass

def answer_with_message(message: str):
    comm_interface = NoisyChannel()
    asyncio.run(comm_interface.send(message))
    return "Message sent"

if __name__ == "__main__":

    message = input("Leave a message after the signal. BEEEEP!")
    response = answer_with_message(message)
    print(response)
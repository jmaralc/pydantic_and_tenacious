import random

class NoisyChannel:

    def send(self, message: str):
        if random.choice([True, False]):
            raise Exception("Connection is broken!")


def answer_with_message(message: str):
    comm_interface = NoisyChannel()
    comm_interface.send(message)
    return "Message sent"

if __name__ == "__main__":
    

    message = input("Leave a message after the signal. BEEEEP!")
    response = answer_with_message(message)
    print(response)
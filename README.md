# About me...
Javier Martinez Alcantara, Javi.

[LinkedIn](https://www.linkedin.com/in/javier-martinez-alcantara/)

[Some info](https://jmaralc.github.io/)

# Be pydantic and tenacious...to reach your goals

The idea of this project is to present different examples of pydantic and a bit of tenacity. The goal is to share the potential of these two packages.

# Motivation

Nowadays there are a lot of tasks that are time consuming and error prone.`hints` in python together with other packages may help you to develop robust services.

[Some related slides](https://docs.google.com/presentation/d/1_GS1WKmKcw8-Ty28G3JBhiv4rbqf2UMaU1hlzc7m0wI/edit?usp=sharing)

# Setup

For this talk you will need python 3.8 and pip installed in your computer. We will use pipenv for installing the packages and keeping the virtual environment. So I recomend you to install it with pip:

```shell
pip install --user --upgrade pipenv
```
Further information about installation [here](https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv)

# Global Goal

Spread the knowledge of python, FastAPI and design of code skills.

# Structure of the repository/project

The repository presents a set of folders that introduce the different examples we are going to review during the presentation:

**background** a simple exercise to show how typing is achieved in python

**hints** the folder that contains the code that introduces hints in the meeting.

**pydantic** is a set of examples, each of them introduce a feature of pydantic and try to show the adventages.

**tenacious** is a set of files each one showing a scenario to demostrate the usage of tenacity.


# How to run it
First set in place pipenv with:

```shell
pipenv install
```

To execute one of the examples, for instance the initial background, just type:
```shell
pipenv run python ./background/typing.py
```

# Special thanks to..
For our beloved pydantic:
https://github.com/samuelcolvin/pydantic/

For the tenacity:
https://github.com/jd/tenacity

For the opportunity to share it with you:
https://codurance.com/

For the nice framework:
https://fastapi.tiangolo.com/